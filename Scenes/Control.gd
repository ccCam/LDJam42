extends Control

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	hide()

func _input(event):
	if Input.is_action_pressed("esc"):
		get_tree().paused = true
		self.show()
		
#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func _on_ResumeButton_pressed():
	self.hide()
	get_tree().paused = false

func _on_QuitButton_pressed():
	get_tree().quit()
